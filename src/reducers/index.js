import error from './error'
import collections from './collections'
import fragments from './fragments'
import currentUser from './current_user'
import users from './users'
import teams from './teams'
import fileUpload from './fileUpload'

export default { error, collections, fragments, currentUser, users, teams, fileUpload }
