# Build status

[![build status](https://gitlab.coko.foundation/pubsweet/pubsweet-client/badges/master/build.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-client/builds)

# Description

This is the PubSweet client, to be used as a dependency in PubSweet apps, such as: https://gitlab.coko.foundation/pubsweet/science-blogger

# Short-term roadmap

- 0.8.0 - Current version
- 0.9.0 - Simple realtime updates of fragments and collections